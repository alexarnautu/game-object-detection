\select@language {romanian}
\select@language {romanian}
\contentsline {chapter}{\numberline {1}Introducere}{3}
\contentsline {section}{\numberline {1.1}Motiva\IeC {{\textcommabelow t}}ie}{4}
\contentsline {chapter}{\numberline {2}Concepte}{6}
\contentsline {section}{\numberline {2.1}Machine Learning}{6}
\contentsline {section}{\numberline {2.2}\IeC {\^I}nv\IeC {\u a}\IeC {{\textcommabelow t}}are supervizat\IeC {\u a}}{8}
\contentsline {section}{\numberline {2.3}Perceptron}{11}
\contentsline {subsection}{\numberline {2.3.1}Neuron Sigmoid}{14}
\contentsline {section}{\numberline {2.4}Re\IeC {{\textcommabelow t}}ele neuronale}{17}
\contentsline {section}{\numberline {2.5}Re\IeC {{\textcommabelow t}}ele neuronale convolu\IeC {{\textcommabelow t}}ionale}{19}
\contentsline {paragraph}{Zone locale receptive}{19}
\contentsline {paragraph}{Coeficien\IeC {{\textcommabelow t}}i \IeC {{\textcommabelow s}}i bias \IeC {\^\i }mp\IeC {\u a}rt\IeC {\u a}\IeC {{\textcommabelow s}}i\IeC {{\textcommabelow t}}i}{23}
\contentsline {paragraph}{Straturile de pooling}{24}
\contentsline {chapter}{\numberline {3}Tema abordat\IeC {\u a}}{27}
\contentsline {chapter}{\numberline {4}Aplica\IeC {{\textcommabelow t}}ie}{28}
\contentsline {section}{\numberline {4.1}Tehnologii}{28}
\contentsline {paragraph}{Python}{28}
\contentsline {paragraph}{Tensorflow}{29}
\contentsline {paragraph}{Keras}{29}
\contentsline {paragraph}{GTK}{29}
\contentsline {section}{\numberline {4.2}Structura}{30}
\contentsline {paragraph}{models}{30}
\contentsline {paragraph}{train}{31}
\contentsline {paragraph}{validation}{31}
\contentsline {paragraph}{config.json}{31}
\contentsline {paragraph}{preprocessing.py}{31}
\contentsline {paragraph}{recorder.py}{31}
\contentsline {paragraph}{constants.py}{32}
\contentsline {paragraph}{predict.py}{32}
\contentsline {paragraph}{train.py}{32}
\contentsline {paragraph}{utils.py}{32}
\contentsline {section}{\numberline {4.3}Detalii de implementare}{33}
\contentsline {subsection}{\numberline {4.3.1}Detec\IeC {{\textcommabelow t}}ie unificat\IeC {\u a}}{34}
\contentsline {subsection}{\numberline {4.3.2}Arhitectura re\IeC {{\textcommabelow t}}elei}{35}
\contentsline {subsection}{\numberline {4.3.3}\IeC {\^I}nv\IeC {\u a}\IeC {{\textcommabelow t}}are}{35}
\contentsline {section}{\numberline {4.4}Antrenarea re\IeC {{\textcommabelow t}}elei}{37}
\contentsline {section}{\numberline {4.5}Rezultate}{38}
\contentsline {chapter}{\numberline {5}Aplicabilitate}{39}
\contentsline {section}{\numberline {5.1}Ma\IeC {{\textcommabelow s}}ini autonome}{40}
\contentsline {section}{\numberline {5.2}Drone}{41}
\contentsline {chapter}{\numberline {6}Concluzie}{42}
