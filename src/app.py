import gi
import time
import logging
import cv2
import argparse
import multiprocessing as mp

gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')

from gi.repository import Gdk, Gtk, GLib, GObject
from recorder.recorder import Recorder, get_pixbuf_from_screen

from detector.utils import config
from detector.utils.preprocessing import parse_annotation
from detector.utils.utils import draw_boxes_and_labels_on_image

from detector.models.yolo import YOLO

parser = argparse.ArgumentParser(
    description="Train and predict YOLO v2 on a dataset.")
parser.add_argument('--train', help="Train on a given dataset", action='store_true')

parser.add_argument('--predict', help="Predict the objects from a given dataset. ", action='store_true')
parser.add_argument('--image', help="Image to predict the objects on")


#logging.basicConfig(filename="/home/arnispyhce/Projects/game-object-detection/logs/debug.log", format='%(levelname)s %(asctime)s %(message)s',level=logging.DEBUG)

# TF log
#tf_log = logging.getLogger('tensorflow')
# create formatter and add it to the handlers
#formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create file handler which logs even debug messages
#fh = logging.FileHandler('/home/arnispyhce/Projects/game-object-detection/logs/tensorflow.log')
#fh.setLevel(logging.DEBUG)
#fh.setFormatter(formatter)
#tf_log.addHandler(fh)
#logger = logging.getLogger(__name__)

"""
def start_detector(to_predict, predicted):
    logger.info("Application started")
    parserargs = parser.parse_args()
    if parserargs.train:
        train_images, train_labels = parse_annotation(config.TRAIN_LABELS_PATH, config.TRAIN_IMAGES_PATH, config.LABELS)

        valid_images, valid_labels = parse_annotation(config.VALID_LABELS_PATH, config.VALID_IMAGES_PATH, config.LABELS)
        yolo = YOLO()
        yolo.train(train_images, valid_images)
    elif parserargs.predict:
        image_path = parserargs.image

        yolo = YOLO()
        yolo.load_weights()
        while True:
            image = cv2.read(to_predict.pop())
            boxes = yolo.predict(image)
            image = draw_boxes_and_labels_on_image(image, boxes, config.LABELS)
            output_image = None

            cv2.imencode('jpg', output_image, image)
            predicted.put(output_image)
        cv2.imwrite(image_path[:-4] + '_detected' + image_path[-4:], image)
    else:
        print("You haven't provided any option. Use --help for more information ")

"""

def start_recorder(to_predict, predicted):
    recorder = Recorder()
    recorder.connect('delete-event', Gtk.main_quit)
    recorder.show_all()


    Gtk.main()
    while True:
#         to_predict.put(get_pixbuf_from_screen())
        #recorder.image.set_from_pixbuf(predicted.pop())
        time.sleep(0.1)



if __name__ == '__main__':
    mp.set_start_method('spawn')
    to_predict = mp.Queue()
    predicted = mp.Queue()

    recorder = mp.Process(target=start_recorder, args=(to_predict, predicted))
    recorder.start()
    recorder.join()
