import matplotlib.pyplot as plt
import numpy as np

def plot_graph(losses, loss_key, loss_label):
    y_axis = np.linspace(
        min(losses[loss_key]),
        max(losses[loss_key]),
        num=len(losses[loss_key])
    )

    x_axis = range(1, len(losses[loss_key]) + 1)

    fig, ax = plt.subplots()
    ax.plot(x_axis, losses[loss_key])
    ax.set(xlabel='Step', ylabel=loss_label,
           title=f'{loss_label} / step')
    
    ax.set_yticks(y_axis, minor=False)
    ax.grid()
    fig.savefig(f"{loss_key}.png")
#    plt.show()

def strip_line(value):
    return value.replace('\t', '').replace('[', '').replace('\n', '').replace(']', '')

if __name__ == '__main__':
    losses = {
        'loss_xy': [],
        'loss_wh': [],
        'loss_conf': [],
        'loss_class': [],
        'total_loss': []
    }
    with open('../tensor.log') as log:
        for line in log.readlines():
            splitted_line = line.split(' ')

            if splitted_line[0] == 'Loss' and splitted_line[1] == 'XY':
                losses['loss_xy'].append(float(strip_line(splitted_line[2])))
            if splitted_line[0] == 'Loss' and splitted_line[1] == 'WH':
                losses['loss_wh'].append(float(strip_line(splitted_line[2])))
            if splitted_line[0] == 'Loss' and splitted_line[1] == 'Conf':
                losses['loss_conf'].append(float(strip_line(splitted_line[2])))
            if splitted_line[0] == 'Loss' and splitted_line[1] == 'Class':
                losses['loss_class'].append(float(strip_line(splitted_line[2])))
            if splitted_line[0] == 'Total' and splitted_line[1] == 'Loss':
                losses['total_loss'].append(float(strip_line(splitted_line[2])))

    plot_graph(losses, 'loss_xy', 'Loss XY')
    plot_graph(losses, 'loss_wh', 'Loss WH') 
    plot_graph(losses, 'loss_conf', 'Loss Confidence')
    plot_graph(losses, 'loss_class', 'Loss Class')
    plot_graph(losses, 'total_loss', 'Total Loss')
