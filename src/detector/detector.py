import logging
import cv2
import argparse

from models.yolo import YOLO
from app import config
from app.preprocessing import parse_annotation

from app.utils import draw_boxes_and_labels_on_image

parser = argparse.ArgumentParser(
    description="Train and predict YOLO v2 on a dataset.")
parser.add_argument('--train', help="Train on a given dataset", action='store_true')

parser.add_argument('--predict', help="Predict the objects from a given dataset. ", action='store_true')
parser.add_argument('--image', help="Image to predict the objects on")


logging.basicConfig(filename="logs/debug.log", format='%(levelname)s %(asctime)s %(message)s',level=logging.DEBUG)

# TF log
tf_log = logging.getLogger('tensorflow')
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create file handler which logs even debug messages
fh = logging.FileHandler('/home/arnispyhce/Projects/game-object/detection/logs/tensorflow.log')
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)
tf_log.addHandler(fh)
logger = logging.getLogger(__name__)

if __name__ == '__main__':
    logger.info("Application started")
    parserargs = parser.parse_args()
    if parserargs.train:
        train_images, train_labels = parse_annotation(config.TRAIN_LABELS_PATH, config.TRAIN_IMAGES_PATH, config.LABELS)

        valid_images, valid_labels = parse_annotation(config.VALID_LABELS_PATH, config.VALID_IMAGES_PATH, config.LABELS)
        yolo = YOLO()
        yolo.train(train_images, valid_images)
    elif parserargs.predict:
        image_path = parserargs.image

        yolo = YOLO()
        yolo.load_weights()

        image = cv2.imread(image_path)
        boxes = yolo.predict(image)
        image = draw_boxes_and_labels_on_image(image, boxes, config.LABELS)

        print(len(boxes), 'boxes are found')

        cv2.imwrite(image_path[:-4] + '_detected' + image_path[-4:], image)
    else:
        print("You haven't provided any option. Use --help for more information ")

    
