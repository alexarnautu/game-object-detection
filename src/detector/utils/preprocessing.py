import os

import xml.etree.ElementTree as ET


def parse_annotation(annotation_directory, image_directory, labels=[]):
    images = []
    seen_labels = {}

    for annotation_file in sorted(os.listdir(annotation_directory)):
        image_data = {'object': []}
        annotation_xml = ET.parse(annotation_directory + annotation_file)

        for e in annotation_xml.iter():
            if 'filename' in e.tag:
                image_data['filename'] = image_directory + e.text
            if 'width' in e.tag:
                image_data['width'] = int(e.text)
            if 'height' in e.tag:
                image_data['height'] = int(e.text)
            if 'object' in e.tag:
                image_obj = {}

                for attr in list(e):
                    if 'name' in attr.tag:
                        image_obj['name'] = attr.text

                        if image_obj['name'] in seen_labels:
                            seen_labels[image_obj['name']] += 1
                        else:
                            seen_labels[image_obj['name']] = 1

                        if len(labels) > 0 and image_obj['name'] not in labels:
                            break
                        else:
                            image_data['object'] += [image_obj]

                    if 'bndbox' in attr.tag:
                        for dimension in list(attr):
                            if 'xmin' in dimension.tag:
                                image_obj['x_min'] = int(round(float(dimension.text)))
                            if 'xmax' in dimension.tag:
                                image_obj['x_max'] = int(round(float(dimension.text)))
                            if 'ymin' in dimension.tag:
                                image_obj['y_min'] = int(round(float(dimension.text)))
                            if 'ymax' in dimension.tag:
                                image_obj['y_max'] = int(round(float(dimension.text)))

        if len(image_data['object']) > 0:
            images += [image_data]

    return images, seen_labels
