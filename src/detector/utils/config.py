WEIGHTS_FILE_PATH = '/home/arnispyhce/Projects/game-object-detection/data/weights.h5'

YOLO_FILE_PATH = '/home/arnispyhce/Projects/game-object-detection/data/yolo.h5'
TRAIN_IMAGES_PATH = '/home/arnispyhce/Projects/game-object-detection/data/labeled/train/images/'
TRAIN_LABELS_PATH = '/home/arnispyhce/Projects/game-object-detection/data/labeled/train/labels/'
VALID_IMAGES_PATH = '/home/arnispyhce/Projects/game-object-detection/data/labeled/validation/images/'
VALID_LABELS_PATH = '/home/arnispyhce/Projects/game-object-detection/data/labeled/validation/labels/'  

LOG_FILE = '/home/arnispyhce/Projects/game-object-detection/logs/'

# Objects
LABELS = ['car']
NO_LABELS = len(LABELS)
MAX_BOX_PER_IMAGE = 10
ANCHORS = [0.57273, 0.677385, 1.87446, 2.06253, 3.33843, 5.47434, 7.88282, 3.52778, 9.77052, 9.16828]
NO_BOXES = len(ANCHORS) // 2

# Training
IMAGE_SIZE = 416

BATCH_SIZE = 1
TRAIN_TIMES = 1
NO_EPOCHS = 1
WARMUP_EPOCHS = 0

LEARNING_RATE = 1e-4
OBJECT_SCALE = 5.0
NO_OBJECT_SCALE = 1.0
COORD_SCALE = 1.0
CLASS_SCALE = 1.0

# Validation
VALID_TIMES = 1
