import numpy as np


class BoundingBox:
    def __init__(self, x_min, y_min, x_max, y_max, c=None, classes=None):
        self.x_min = x_min
        self.y_min = y_min
        self.x_max = x_max
        self.y_max = y_max

        self.c = c
        self.classes = classes

        self.__label = -1
        self.__score = -1

    @property
    def label(self):
        if self.__label == -1:
            self.__label = np.argmax(self.classes)

        return self.__label

    @label.setter
    def label(self, value):
        self.__label = value

    @property
    def score(self):
        if self.__score == -1:
            self.__score = self.classes[self.label]

        return self.__score

    @score.setter
    def score(self, value):
        self.__score = value




