import copy
import cv2

import numpy as np

from keras.utils import Sequence

from imgaug import augmenters as iaa

from detector.utils.config import *
from detector.models.bounding_box import BoundingBox
from detector.utils.utils import compute_box_iou


class BatchGenerator(Sequence):
    def __init__(self, images, grid, shuffle=None, norm=None, jitter=None):
        self.generator = None

        self.images = images
        self.shuffle = shuffle
        self.grid = grid
        self.norm = norm

        self.jitter = jitter

        print(range(int(len(ANCHORS))))

        self.anchors = [BoundingBox(0, 0, ANCHORS[2 * i], ANCHORS[2 * i + 1]) for i in
                        range(int(len(ANCHORS) // 2))]

        sometimes = lambda aug: iaa.Sometimes(0.5, aug)

        self.aug_pipe = iaa.Sequential([
            sometimes(iaa.Affine()),
            iaa.SomeOf((0, 5), [
                iaa.OneOf([
                    iaa.GaussianBlur((0, 3.0)),
                    iaa.AverageBlur(k=(2, 7)),
                    iaa.MedianBlur(k=(3, 11)),
                ]),
                iaa.Sharpen(alpha=(0, 1.0), lightness=(0.75, 1.5)),
                iaa.AdditiveGaussianNoise(loc=0, scale=(0.0, 0.05 * 255), per_channel=0.5),
                iaa.OneOf([
                    iaa.Dropout((0.01, 0.1), per_channel=0.5),  # randomly remove up to 10% of the pixels
                    # iaa.CoarseDropout((0.03, 0.15), size_percent=(0.02, 0.05), per_channel=0.2),
                ]),
                iaa.Add((-10, 10), per_channel=0.5),
                # change brightness of images (by -10 to 10 of original value)
                iaa.Multiply((0.5, 1.5), per_channel=0.5),
                # change brightness of images (50-150% of original value)
                iaa.ContrastNormalization((0.5, 2.0), per_channel=0.5),  # improve or worsen the contrast
            ], random_order=True),

        ], random_order=True)

        if shuffle:
            np.random.shuffle(self.images)

    def __len__(self):
        return int(np.ceil(float(len(self.images)) / BATCH_SIZE))

    def num_classes(self):
        return len(LABELS)

    def size(self):
        return len(self.images)

    def load_annotation(self, i):
        annotations = []
        for obj in self.images[i]['object']:
            annotations.extend([obj['x_min'], obj['y_min'], obj['x_max'], obj['y_max'],
                                LABELS.index(obj['name'])])

        if not annotations:
            annotations = [[]]
        return np.array(annotations)

    def load_image(self, i):
        return cv2.imread(self.images[i]['filename'])

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.images)

    def aug_image(self, train_instance, jitter):
        image = cv2.imread(train_instance['filename'])
        height, width, c = image.shape
        objects = copy.deepcopy(train_instance['object'])

        if jitter:
            ### scale the image
            scale = np.random.uniform() / 10. + 1.
            image = cv2.resize(image, (0, 0), fx=scale, fy=scale)

            ### translate the image
            max_offx = (scale - 1.) * width
            max_offy = (scale - 1.) * height
            offset_x = int(np.random.uniform() * max_offx)
            offset_y = int(np.random.uniform() * max_offy)

            image = image[offset_y: (offset_y + height), offset_x: (offset_x + width)]

            ### flip the image
            flip = np.random.binomial(1, .5)
            if flip > 0.5: image = cv2.flip(image, 1)

            image = self.aug_pipe.augment_image(image)

        image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE))

        for obj in objects:
            for attribute in ['x_min', 'x_max']:
                if jitter:
                    obj[attribute] = int(obj[attribute] * scale - offset_x)

                obj[attribute] = int(obj[attribute] * float(IMAGE_SIZE) / width)
                obj[attribute] = max(min(obj[attribute], IMAGE_SIZE), 0)

            for attribute in ['y_min', 'y_max']:
                if jitter: obj[attribute] = int(obj[attribute] * scale - offset_y)

                obj[attribute] = int(obj[attribute] * float(IMAGE_SIZE) / height)
                obj[attribute] = max(min(obj[attribute], IMAGE_SIZE), 0)

            if jitter and flip > 0.5:
                x_min = obj['x_min']
                obj['x_min'] = IMAGE_SIZE - obj['x_max']
                obj['x_max'] = IMAGE_SIZE - x_min

        return image, objects

    def __getitem__(self, idx):
        left_bound = idx * BATCH_SIZE
        right_bound = (idx + 1) * BATCH_SIZE

        if right_bound > len(self.images):
            right_bound = len(self.images)
            left_bound = right_bound - BATCH_SIZE

        instance_count = 0
        bound_diff = right_bound - left_bound

        x_batch = np.zeros((bound_diff, IMAGE_SIZE, IMAGE_SIZE, 3))
        b_batch = np.zeros((bound_diff, 1, 1, 1, MAX_BOX_PER_IMAGE, 4))
        y_batch = np.zeros((bound_diff, self.grid['height'], self.grid['width'], NO_BOXES, 4 + 1 + len(LABELS)))

        for train_instance in self.images[left_bound:right_bound]:
            image, all_objects = self.aug_image(train_instance, self.jitter)

            true_box_index = 0
            for obj in all_objects:
                if obj['x_max'] > obj['x_min'] and obj['y_max'] > obj['y_min'] and obj['name'] in LABELS:
                    center_x = .5 * (obj['x_min'] + obj['x_max'])
                    center_x = center_x / (float(IMAGE_SIZE) / self.grid['width'])

                    center_y = .5 * (obj['y_min'] + obj['y_max'])
                    center_y = center_y / (float(IMAGE_SIZE) / self.grid['height'])

                    grid_x = int(np.floor(center_x))
                    grid_y = int(np.floor(center_y))

                    if grid_x < self.grid['width'] and grid_y < self.grid['height']:
                        obj_index = LABELS.index(obj['name'])

                        center_width = (obj['x_max'] - obj['x_min']) / (float(IMAGE_SIZE) / self.grid['width'])
                        center_height = (obj['y_max'] - obj['y_min']) / (float(IMAGE_SIZE) / self.grid['height'])

                        box = [center_x, center_y, center_width, center_height]

                        # Find the anchors that predicts this box
                        best_anchor = -1
                        max_iou = -1
                        shifted_box = BoundingBox(0, 0, center_width, center_height)

                        for index in range(len(self.anchors)):
                            anchor = self.anchors[index]
                            iou = compute_box_iou(shifted_box, anchor)

                            if max_iou < iou:
                                best_anchor = index
                                max_iou = iou
                        # Assign the ground truth x, y, w, h confidence and class probabilities to y_batch
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 0:4] = box
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 4] = 1.
                        y_batch[instance_count, grid_y, grid_x, best_anchor, 5 + obj_index] = 1

                        # Assign the true box to b_batch
                        b_batch[instance_count, 0, 0, 0, true_box_index] = box

                        true_box_index += 1
                        true_box_index = true_box_index % MAX_BOX_PER_IMAGE

                if self.norm:
                    x_batch[instance_count] = self.norm(image)
                else:
                    # Plot image and bounding boxes for sanity check
                    for obj in all_objects:
                        if obj['x_max'] > obj['x_min'] and obj['y_max'] > obj['y_min']:
                            cv2.rectangle(image[:, :, ::-1], (obj['x_min'], obj['y_min']),
                                          (obj['x_max'], obj['y_max']), (255, 0, 0), 3)
                            cv2.putText(image[:, :, ::-1], obj['name'],
                                        (obj['x_min'] + 2, obj['y_min'] + 12),
                                        0, 1.2e-3 * image.shape[0],
                                        (0, 255, 0), 2)

                    x_batch[instance_count] = image
                instance_count += 1
            return [x_batch, b_batch], y_batch






