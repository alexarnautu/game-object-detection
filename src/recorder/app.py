import gi
import time
import threading
from datetime import datetime

from config import config

gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')

from gi.repository import Gdk, Gtk, GLib, GObject


def get_pixbuf_from_screen():
    window = Gdk.get_default_root_window()
    pixbuf = Gdk.pixbuf_get_from_window(window, 0, 54, 800, 600)
    return pixbuf


class Recorder(Gtk.Window):
    def __init__(self):
        self.label_lock = threading.Lock()

        Gtk.Window.__init__(self, title="Recorder")

        self.set_border_width(10)
        main_layout = Gtk.VBox(spacing=6)
        self.add(main_layout)

        hbox = Gtk.HBox()
        main_layout.add(hbox)

        self.button = Gtk.Button.new_with_label("Start")
        hbox.pack_start(self.button, True, True, 0)
        self.button.connect('clicked', self.start_recording)

        self.label = Gtk.Label(label="Recording not started")
        hbox.pack_start(self.label, True, True, 0)

        vbox = Gtk.VBox()
        main_layout.add(vbox)

        self.image = Gtk.Image.new_from_pixbuf(get_pixbuf_from_screen())
        vbox.pack_start(self.image, True, True, 0)

        thread = threading.Thread(target=self.reload_image)
        thread.daemon = True
        thread.start()

    def update_image(self):
        while True:
            
if __name__ == '__main__':
    recorder = Recorder()
    recorder.connect('delete-event', Gtk.main_quit)
    recorder.show_all()

    Gtk.main()
